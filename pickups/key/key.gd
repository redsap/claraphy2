extends Area2D

export(int) var id = 0

func _ready():
	connect("body_entered", self, "body_entered")
	
func body_entered(body):
	if body.name == "player":
		global_state.add_key()
		queue_free()