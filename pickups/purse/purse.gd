extends Area2D

export(int) var value = 50

func _ready():
	connect("body_entered", self, "body_entered")
	pass
	
		
func body_entered(body):
	if body.name == "player":
		global_state.add_wealth(value)
		queue_free()