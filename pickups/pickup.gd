extends Area2D

export(int) var id = 0
export(int) var gold_amount = 0
export(int) var mana_amount = 0
export(int) var health_amount = 0

func _ready():
	connect("body_entered", self, "body_entered")
	
func body_entered(body):
	if body.name == "player":
		global_state.add_mana(mana_amount)
		global_state.add_wealth(gold_amount)
		global_state.add_health(health_amount)
		queue_free()
