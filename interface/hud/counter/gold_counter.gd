extends Label

func _ready():
	text = str(global_state.wealth)
	signal_manager.connect("player_wealth_changed", self, "_player_wealth_callback")
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
func _player_wealth_callback(value):
	text = str(value)
