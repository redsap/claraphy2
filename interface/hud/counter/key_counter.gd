extends Label

func _ready():
	text = str(global_state.keys)
	signal_manager.connect("key_count_changed", self, "_key_count_callback")
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
func _key_count_callback(value):
	text = str(value)