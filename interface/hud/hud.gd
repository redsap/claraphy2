extends CanvasLayer

const HEART_MAX = 6 # Maximum number of whole hearts
const HEART_ROW_SIZE = 10
const HEART_OFFSET = 9

const MANA_MAX = 41 # Number of mana bars
const MANA_X_POS = 0

func _ready():
	init_hearts()
	init_mana()
		
func _process(delta):
	update_hearts()
	update_mana()

func init_hearts():
	for i in HEART_MAX:
		var new_heart = Sprite.new()
		new_heart.texture = $hearts.texture
		new_heart.hframes = $hearts.hframes
		$hearts.add_child(new_heart)
	
func update_hearts():
	for heart in $hearts.get_children():
		var index = heart.get_index() # Index is the current whole heart sprite, zero based
		var x = (index % HEART_ROW_SIZE) * HEART_OFFSET
		var y = (index / HEART_ROW_SIZE) * HEART_OFFSET
		heart.position = Vector2(x, y)
		
		# Each heart has four frames. A full heart is 1 health, half a heart is 0.5 health.
		# Index is zero based
		var full_hearts = floor(global_state.health)
		if index > full_hearts: # Not enough health, empty heart
			heart.frame = 0
		if index == full_hearts: # Calculate part of a heart
			heart.frame = (global_state.health - full_hearts) * 4 # Show a full heart
		if index < full_hearts: # Plenty of health, full heart
			heart.frame = 4

func init_mana():
	for i in MANA_MAX:
		var new_mana = Sprite.new()
		new_mana.texture = load("res://interface/hud/Mana.png")
		new_mana.visible = false
		new_mana.position = Vector2(83 + i, 11)
		$mana.add_child(new_mana)
					
func update_mana():
	var limit = min((global_state.mana / 100.0) * MANA_MAX, MANA_MAX)
	for mana in $mana.get_children():
		var index = mana.get_index()
		mana.visible = (index < limit)
