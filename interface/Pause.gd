extends Control


func _input(event):
	if event.is_action_pressed("pause"):
		global_state.toggle_pause()
		visible = global_state.pause_state