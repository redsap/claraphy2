extends Camera2D

var player

func _ready():
	$area.connect("body_entered", self, "body_entered")
	$area.connect("body_exited", self, "body_exited")
	player = get_tree().get_root().find_node("player", true, false)

func _process(delta):
	var pos = player.global_position - Vector2(0,16)
	var x = floor(pos.x / 432) * 432
	var y = floor(pos.y / 256) * 256
	global_position = Vector2(x,y)

func body_entered(body):
	if body.get("TYPE") == "ENEMY":
		body.set_physics_process(true)
	
func body_exited(body):
	if body.get("TYPE") == "ENEMY":
		body.set_physics_process(false)