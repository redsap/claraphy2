extends Position2D

var grid_size = Vector2()
var grid_position = Vector2(1,1)

onready var parent = get_parent()

func _ready():
	$area.connect("body_entered", self, "body_entered")
	$area.connect("body_exited", self, "body_exited")

	grid_size = Vector2(432,256)
	set_as_toplevel(true)
	update_grid_position()
	
func _physics_process(delta):
	update_grid_position()
	
func update_grid_position():
	var x = floor(parent.position.x / grid_size.x)
	var y = floor(parent.position.y / grid_size.y)
	var new_grid_position = Vector2(x,y)
	if grid_position == new_grid_position:
		return
	grid_position = new_grid_position
	print(str("player gobal pos", parent.position))
	print(str("grid pos", grid_position))
	position = grid_position * grid_size
	
func body_entered(body):
	print(str(body.name))
	if body.get("TYPE") == "ENEMY":
		body.set_physics_process(true)
	
func body_exited(body):
	if body.get("TYPE") == "ENEMY":
		body.set_physics_process(false)