extends "res://actors/actor.gd"

var anim
var new_anim
var game
var state
enum {ATTACKING_NONE, ATTACKING_STAB, ATTACKING_SWIPE}
var attacking = ATTACKING_NONE

func _ready():
	if self.HEALTH > 0: # Only override the global state health if the player's health is greater then 0
		global_state.health = self.HEALTH
	global_state.mana = self.MANA

func _physics_process(delta):
	controls_loop()
	
	if attacking != ATTACKING_NONE: return

	movement_loop()
	spritedir_loop()
	damage_loop()
	
	if is_on_wall():
		if spritedir == "left" and test_move(transform, Vector2(-1,0)):
			anim_switch("push_")
		if spritedir == "right" and test_move(transform, Vector2(1,0)):
			anim_switch("push_")
		if spritedir == "up" and test_move(transform, Vector2(0,-1)):
			anim_switch("push_")
		if spritedir == "down" and test_move(transform, Vector2(0,1)):
			anim_switch("push_")
	elif movedir != Vector2(0,0):
		anim_switch("walk_")
	else:
		anim_switch("idle_")

func controls_loop():
	var left = Input.is_action_pressed('ui_left')
	var right = Input.is_action_pressed('ui_right')
	var up = Input.is_action_pressed('ui_up')
	var down = Input.is_action_pressed('ui_down')
	var stab = Input.is_action_just_pressed('stab')
	var swipe = Input.is_action_just_pressed('swipe')

	movedir.x = -int(left) + int(right)
	movedir.y = -int(up) + int(down)
	
	if !left and !right and !up and !down and (attacking == ATTACKING_NONE):
		if stab:
			attacking = ATTACKING_STAB
			$attack_timer.start()
			anim_switch("stab_")
		if swipe:
			if global_state.mana > 0:
				global_state.use_mana(10)
				attacking = ATTACKING_SWIPE
				$attack_timer.start()
				anim_switch("swipe_")

			
func hit(damage):
	if $damage_timer.is_stopped():
		global_state.use_health(damage)
		$damage_timer.start()


func _on_damage_timer_timeout():
	$damage_timer.stop()
	pass # replace with function body
	
func use_item(item):
	pass
	#var new_item = item.instance()
	#new_item.add_to_group(str(new_item.get_name(), self))
	#add_child(new_item)


func _on_anim_animation_finished(anim_name):
	#if anim_name.begins_with("swipe") or anim_name.begins_with("stab"):
	#	attacking = ATTACKING_NONE
	pass # replace with function body


func _on_anim_animation_started(anim_name):
	#if anim_name.begins_with("swipe") or anim_name.begins_with("stab"):
	#	attacking = true
	pass # replace with function body


func _on_attack_timer_timeout():
	attacking = ATTACKING_NONE
	pass # replace with function body
