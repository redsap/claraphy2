extends Node2D

export(int) var damage = 1

func _on_area_area_entered(area):
	if area.name == "hitbox" && area.get_parent().has_method("hurt"):
		area.get_parent().hurt(damage)
	pass # replace with function body
