extends "res://actors/actor.gd"

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export(Vector2) var direction = Vector2(0.5,0)

func _ready():
	$anim.play("default")

func _physics_process(delta):
	if HEALTH <= 0:
		queue_free()

	damage_loop()

	var collision = move_and_collide(direction)
	if collision:
		direction = -direction
