extends KinematicBody2D

# An actor has a speed which is configurable in the UI while designing the game
export(int) var SPEED = 0
# Actors have a type to identify an actor when not in view
# e.g. "ENEMY" or "PLAYER"
export(String) var TYPE = "ENEMY"
# The subtype is used to identify the type, mostly of enemies
export(String) var SUBTYPE = ""
# The initial health of an actor in number of hearts, can be fractions in multiples of 1/4
# i.e. 0.25 health is a quarter of a heart
export(float) var HEALTH = 0
# The damage this actor causes another actor when touched, damage is related to health
export(float) var DAMAGE = 0.25
# The starting mana value in percent, applies to player and enemies
export(int) var MANA = 0

var movedir = Vector2(0,0)
var knockdir = Vector2(0,0)
var spritedir = "down"
var hitstun = 1

func _ready():
	if TYPE == "ENEMY":
		# Pause this actor until it is in view
		# The camera will set the physics process to false when the actor goes out of view
		# and sets the physics process to true when the actor comes into view
		set_physics_process(false)

# Apply the amount of damage to the actor
func hurt(damage):
	HEALTH -= damage

# Move the actor based on the values set by previous functions
func movement_loop():
	var motion
	if hitstun == 0:
		motion = movedir.normalized() * SPEED
	else:
		motion = knockdir.normalized() * SPEED * 1.5
	move_and_slide(motion, Vector2(0,0))

# Change the direction of the sprite according to the current movement direction
func spritedir_loop():
	match movedir:
		Vector2(-1,0):
			spritedir = "left"
		Vector2(1,0):
			spritedir = "right"
		Vector2(0,-1):
			spritedir = "up"
		Vector2(0,1):
			spritedir = "down"
			
# Change animation if it is different
func anim_switch(animation):
	var newanim = str(animation, spritedir)
	if $anim.current_animation != newanim:
		$anim.play(newanim)

# Process any damage including being stunned, if being stunned, don't get damaged
func damage_loop():
	if hitstun > 0:
		hitstun -= 1
	else:
		for body in $hitbox.get_overlapping_bodies():
			if (body.get("DAMAGE") != null) and (body.get("TYPE") != TYPE):
				global_state.add_health(-body.get("DAMAGE"))
				hitstun = 4
				knockdir = (transform.origin - body.transform.origin)
