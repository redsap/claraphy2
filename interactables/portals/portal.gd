extends Area2D

export(NodePath) var destination = null


func _on_portal_body_entered(body):
	if (body.name == "player" and not global_state.porting):
		global_state.porting = true
		var dest_pos = get_node(destination).global_position
		body.global_position = dest_pos

func _on_portal_body_exited(body):
	global_state.porting = false
