extends "res://scripts/chest.gd"

export var gold_amount = 10

func take_reward():
	.take_reward()
	$open/timer.start()
	global_state.add_wealth(gold_amount)



func _on_timer_timeout():
	$open/contents.visible = false
	pass # replace with function body
