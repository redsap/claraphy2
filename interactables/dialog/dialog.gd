extends CanvasLayer

var in_dialog = false
var dict = null
var step = null

func _ready():
	global_state.dialog = self
	pass

func _physics_process(delta):
	if in_dialog and dict and Input.is_action_just_pressed("ui_accept"):
		if step: # A step is available to be processed
			set_text(step.text)
			if step.next:
				step = dict[step.next]
			else:
				step = null
		else: # There is no step to be processed
			hide()
			global_state.toggle_pause()
			in_dialog = false
			dict = null
			step = null
	
	
func start_dialog(dict):
	if not in_dialog:
		global_state.toggle_pause()
		in_dialog = true
		self.dict = dict
		step = dict["start"]
		show()
	
func set_text(text):
	$popup/label.text = text

func show():
	$popup.popup()

func hide():
	$popup.hide()