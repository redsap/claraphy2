extends StaticBody2D

func _ready():
	$area.connect("body_entered", self, "body_entered")

func _process(delta):
	var enemies = false
	for e in get_node("./").get_children():
		if e.get("TYPE") == "ENEMY":
			enemies = true
	if !enemies:
		queue_free()
	
func body_entered(body):
	if body.name == "player":
		if body.has_method('hit'):
			body.hit(10)

