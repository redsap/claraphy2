<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.2" name="Terrain" tilewidth="16" tileheight="16" tilecount="15" columns="5">
 <image source="../../../Claraphy 2/tiles_dirt.png" width="80" height="48"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="16">
    <polygon points="0,0 0,-16 16,-16 16,-12 4,-12 4,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,3 0,4 16,4 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="2">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,4 12,4 12,16 16,16 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="3">
  <objectgroup draworder="index">
   <object id="1" x="16" y="3">
    <polygon points="0,0 -2,0 -2,13 0,13"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="4">
  <objectgroup draworder="index">
   <object id="1" x="0" y="3">
    <polygon points="0,0 2,0 2,13 0,13"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="5">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,16 4,16 4,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="7">
  <objectgroup draworder="index">
   <object id="1" x="16" y="0">
    <polygon points="0,0 -4,0 -4,16 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="8">
  <objectgroup draworder="index">
   <object id="1" x="14" y="0">
    <polygon points="0,0 0,2 2,2 2,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="9">
  <objectgroup draworder="index">
   <object id="1" x="2" y="0">
    <polygon points="0,0 0,2 -2,2 -2,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="10">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,16 16,16 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="11">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 15,0 16,0 16,16 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="12">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 16,0 16,16 0,16"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
