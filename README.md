# Dialog system

A NPC must be of type Sprite with an Area2D as a child, named Area2D

Sprite
-> area
--> CollisionShape2D

The script must inherit from npc.gd

``` 
extends "res://scripts/npc.gd"
```

Dialog Name must correspond to a JSON file in the res://assets folder. The files have the extension 'json' and won't show up in the file dialog in Godot editor.

If the 'Dialog Name' is 'sign' the system will look for a file with the name 'res://assets/sign.json'.

The json file provides the script
There must always be a "start" object.
There must always be a "text" object.
The "next" object points to the next object.
If an object has a 'null' next object it will be the last step in the dialog.

```json
{
    "start": {
        "text": "Careful",
        "next": "2"
    },
    "2": {
        "text": "Dangerous",
        "next": null
    }
}
```
