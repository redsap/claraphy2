extends Node

export(String) var dialog_name = null

var dialog = null
var player_near = false
var in_dialog = false

func _ready():
	if not $area:
		printerr("Warning: NPC's must have an area node to detect the player.")
	else:
		$area.connect("body_entered", self, "body_entered")
		$area.connect("body_exited", self, "body_exited")
		
	self.read_dialog(dialog_name)
		
func _physics_process(delta):
	if player_near and Input.is_action_just_pressed("ui_accept"):
		print(str("dialog ", global_state.dialog))
		print("should start talking")
		global_state.dialog.start_dialog(dialog)
	pass
	
func read_dialog(name):
	var path = str("res://assets/", name, ".json")
	var f = File.new()
	if not f.file_exists(path):
		printerr(str(path, " not found."))
		return # Error! We don't have a save to load.
	else:
		f.open(path, File.READ)
		var json = JSON.parse(f.get_as_text())
		# It HAS to be of type dictionary
		if (typeof(json.result) == TYPE_DICTIONARY):
			dialog = json.result
		else:
			printerr(str("The dialog files must be JSON objects\n", json.error_string, "\nline: ", json.error_line))
			get_tree().quit()

func body_entered(body):
	if body.get("TYPE") == "PLAYER":
		player_near = true
	
func body_exited(body):
	if body.get("TYPE") == "PLAYER":
		player_near = false
