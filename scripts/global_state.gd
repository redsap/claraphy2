extends Node
# Class to rember the state of the game such as player health, inventory, game progress etc.

# TODO: Store the values in a map, will not use getters/setters
var health setget set_health
var wealth setget set_wealth
var mana setget set_mana # This is a percent value, 100 is 100%
var keys # The number of keys currently picked up
var porting = false

var wealth_goal
var timer

var pause_state = false
var dialog = null

func _init():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	health = 6.0
	wealth = 0
	wealth_goal = 0
	keys = 0
	timer = Timer.new()
	timer.set_wait_time(0.02)
	timer.set_one_shot(false)
	timer.connect("timeout", self, "add_coin")
	add_child(timer)

func add_health(value):
	health += value

func use_health(value):
	health -= value

func set_health(value):
	health = value

func set_wealth(value):
	wealth = value
	wealth_goal = value
	
func add_wealth(value):
	wealth_goal += value
	if timer.is_stopped():
		timer.start()
	
func set_mana(value):
	mana = value

func add_mana(value):
	mana += value

func use_mana(value):
	mana -= value

func add_coin():
	if wealth == wealth_goal:
		timer.stop()
	if wealth > wealth_goal:
		wealth -= 1
		signal_manager.emit_signal("player_wealth_changed", wealth)
	if wealth < wealth_goal:
		wealth += 1
		signal_manager.emit_signal("player_wealth_changed", wealth)
		
func add_key():
	keys += 1
	signal_manager.emit_signal("key_count_changed", keys)
	
func use_key():
	if keys > 0:
		keys -= 1
	
func save():
	# Save state to file
	pass

func toggle_pause():
	pause_state = not get_tree().paused
	get_tree().paused = pause_state
	pass