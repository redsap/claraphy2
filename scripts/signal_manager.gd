extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

signal player_health_changed(health)
signal player_wealth_changed(wealth)
signal player_mana_changed(mana)
signal key_count_changed(keys)

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
