extends Node2D

export(bool) var open = false
var player_adjacent = false

func _ready():
	$area.connect("body_entered", self, "body_entered")
	$area.connect("body_exited", self, "body_exited")
	set_state()
	
func _physics_process(delta):
	if Input.is_action_pressed('ui_select') and (not open) and (player_adjacent):
		take_reward()
	pass

func set_state():
	$closed.visible = not self.open
	$closed.get_node("StaticBody2D/CollisionShape2D").disabled = self.open
	$open.visible = self.open
	$open.get_node("StaticBody2D/CollisionShape2D").disabled = not self.open
	
func body_entered(body):
	if body.name == "player":
		player_adjacent = true
		
func body_exited(body):
	if body.name == "player":
		player_adjacent = false
		
func take_reward():
	self.open = true
	set_state()
	